const getSum = (str1, str2) => {
  if (typeof str1 !== 'string' || typeof str2 !== 'string')
      return false;
  if (Number(str1) || Number(str2)) {
      str1 = Number(str1);
      str2 = Number(str2);
  }
  else if (str1.length === 0 || str2.length === 0) {
      str1 = 0;
      str2 = 0;
  }
  else return false
  return String(str1 + str2);
};

const getQuantityPostsByAuthor = (listOfPosts, authorName) => {
  let countOfPosts = 0;
  let countOfComments = 0;
  for (let key in listOfPosts) {
      if (listOfPosts[key]['author'] === authorName)
          countOfPosts++;
      if (listOfPosts[key].hasOwnProperty('comments')) {
          for (let comKey in listOfPosts[key]['comments']) {
              if (listOfPosts[key]['comments'][comKey]['author'] === authorName)
                  countOfComments++;
          }
      }
  }
  return `Post:${countOfPosts},comments:${countOfComments}`;
};

const tickets = (people) => {  
  if (people.length === 2 && people.hasOwnProperty('acc')) return 'YES';
  else
  {
      if (!people.hasOwnProperty('acc')) {
          people.acc = 0;
      }
      if (people[0] - 25 <= people['acc']) {
          people['acc'] = people['acc'] - 25 + people[0];
          people.slice = [].slice();
          const newArr = [].slice.call(1, people.length);
          tickets(newArr);
      }
      else
          return 'NO';
  }
  return 'YES';
};


module.exports = { getSum, getQuantityPostsByAuthor, tickets };
